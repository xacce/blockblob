-r requirements.txt
flake8==3.3.0
flake8-coding==1.3.0
flake8-deprecated==1.2
flake8-import-order==0.12
flake8-isort==2.2.1
flake8-logging-format==0.2.0
flake8-module-imports==1.1
flake8-polyfill==1.0.1
flake8-print==2.0.2
flake8-quotes==0.11.0
flake8-regex==0.3
flake8-sorted-keys==0.1.0
flake8-string-format==0.2.3
pep8