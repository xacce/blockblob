# coding: utf-8
from __future__ import unicode_literals

import logging
import os
from uuid import uuid4


def notset(fn):
    """ Проверка доступности хранилища и был ли вызван метод init/read перед тем как взаимодействовать с данными

    Returns:
        Вернет 0 если какие-либо условия были не выполнены
    """

    def wrapper(blob, *args, **kwargs):
        if not blob.can_be_manipulated():
            return 0
        return fn(blob, *args, **kwargs)

    return wrapper


class Blob(object):
    """
        Args:
            manifest (Manifest):

        Attributes:
            _manifest (Manifest):
    """

    def __init__(self, manifest):
        """

        Args:
            manifest (Manifest):
        """
        self._blob_size = None
        self._block_size = None
        self._blob_file = None
        self._blob_id = None
        self._block_count = 0
        self._manifest = manifest

    def read(self, blob_id):
        """ Чтение  блоков из указанного блоба

        Args:
            blob_id (str):

        Returns:
            1 в случ. успеха
        """
        self._blob_id = blob_id
        try:
            self._blob_file = open(os.path.join(self._manifest.path, self._blob_id), 'r+')
        except IOError:
            logging.exception('Cant open blob file %s', self._blob_id)
            return 0

        try:
            self._blob_size, self._block_size = map(int, self._blob_file.read(29).strip().split(' '))
        except ValueError:
            logging.exception('Cant read blob data, may be corrupted: %s', self._blob_id)
            return 0

        return 1

    def init(self, blob_size, block_size):
        """ Инициализация нового блоба

        Args:
            blob_size (int): кол-во блоков
            block_size (int): размер блоков

        Returns:
            1 в случ. успеха или 0?
        """
        self._blob_size = blob_size
        self._block_size = block_size

    @notset
    def get_by_pos(self, pos, bts):
        """ Заполнит bts данными блока, см. Manifest.search_block

        Args:
            pos (int): Позиция блока в текущем блобе
            bts (bytearray):

        Returns:
            1 в случ. успеха или 0

        """
        offs = self._block_size * pos - self._block_size
        offs += 30
        self._blob_file.seek(offs)
        bts += self._blob_file.read(self._block_size)
        return 1

    @notset
    def put(self, block_id, block_data):
        """ Вставка нового блока в блоб

        Args:
            block_id (int): Id блока
            block_data (bytearray):

        Returns:
            1 в случ. успеха или 0
        """
        if not len(block_data) == self._block_size:
            logging.critical('Incorrect block length, must be %s, current %s', self._block_size, len(block_data))
            return 0
        hashed = self._manifest.get_hash(block_data)
        if self._manifest.search_block(block_id, hashed):
            logging.info('%s duplicate', block_id)
            return 1
        if self._block_count == self._blob_size:
            logging.critical('Size limit was reached')
            return 0
        self.make_if_not_exists()
        self._block_count += 1  # Отдельный вопрос как должна себя вести система если blob не был забит, т.к все вставленные в него block являются дубликатами других blob
        self._manifest.store_block(self._blob_id, hashed, self._block_count, block_id)
        self._blob_file.write(block_data)
        return 1

    def make_if_not_exists(self):
        """ Данный метод создаст файл блоба если он не был создан, чтобы не плодить файлы при каждой инициализации
        """
        if not self._blob_file:
            self._blob_id = str(uuid4())
            self._blob_file = open(os.path.join(self._manifest.path, self._blob_id), 'wb')
            fl = '{0} {1}\n'.format(self._blob_size, self._block_size)
            fl = fl.rjust(30)
            self._blob_file.write('{0}'.format(fl))

    def can_be_manipulated(self):
        """

        Returns:
            bool: допустимо ли процедуру вставки/чтения блоков
        """
        if not self._block_size:
            logging.critical('Block size must be defined, call read or init function')
            return False
        if not self._blob_size:
            logging.critical('Blob size must be defined, call read or init function')
            return False
        if not self._manifest.good:
            logging.critical('Manifest says: bad. See logs.')
            return False

        return True

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._blob_file:
            self._blob_file.close()

    def __enter__(self):
        return self
