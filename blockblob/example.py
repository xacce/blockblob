# coding: utf-8
from __future__ import unicode_literals
import logging
import os
from random import randint

from blob import Blob
from manifest_filesystem import ManifestFileSystem


def example():
    logging.basicConfig(level=logging.DEBUG)

    if not os.path.exists('/tmp/.meta'):
        open('/tmp/.meta', 'wb').close()
    m = ManifestFileSystem()

    data = {randint(100000000, 9999999999): bytearray(os.urandom(10000000)) for x in xrange(9)}
    duplicate_id = randint(100000000, 9999999999)
    with Blob(manifest=m) as blob:
        blob.init(blob_size=10, block_size=10000000)
        for x, y in data.items():
            blob.put(block_id=x, block_data=y)
        blob.put(*data.items()[5])  # by block id deduplicate
        blob.put(duplicate_id, data.values()[5])  # by hash deduplicate

    print 'Search for', data.keys()[5]
    result = m.search_block(block_id=data.keys()[5])

    with Blob(manifest=m):
        blob.read(result[0])
        bts = bytearray()
        print 'Search result: ', blob.get_by_pos(result[1], bts)
        print 'Is valid: ', bts == data.values()[5]

    result = m.search_block(block_id=duplicate_id)
    with Blob(manifest=m):
        blob.read(result[0])
        bts = bytearray()
        print 'Search result: ', blob.get_by_pos(result[1], bts)
        print 'Is valid: ', bts == data.values()[5]
        print 'Write after read:', blob.put(block_id=randint(100000000, 9999999999), block_data=bytearray(os.urandom(10000000)))
