# coding: utf-8
from __future__ import unicode_literals
import hashlib


class Manifest(object):
    """ Осуществляет поиск по хранилищу и дедупликацию данных

    Args:
        hash_func: Тип хеширования блоков для дальнейшего сравнения (sha256 по умолчанию)
    """

    def __init__(self, hash_func=hashlib.sha256):
        self._good = False
        self._hash_func = hash_func
        self._check()

    def _check(self):
        raise NotImplementedError()

    def store_block(self, blob_id, hashed, position, block_id):
        """  Сохранить данные о блоке в манифест

        Args:
            blob_id (str: id (uuid4) блоба в файловой системе
            hashed (str):  хеш блока
            position (int): позиция блока в блобе
            block_id (int): id блока

        Returns:
            int: 1 в случ. успеха или 0
        """
        raise NotImplementedError()

    def search_block(self, block_id, hashed=None):
        """ Поиск блока по хранилищу

        Args:
            block_id (int): id блока
            hashed (:obj:`str`, optional): хеш блока для поиска по хешу

        Returns:
            stored_blob_id (str): id блоба
            position (int): позиция в блобе

            Вернет 0 если блок не найден
        """

        raise NotImplementedError()

    def get_hash(self, bts):
        """  Вернет хеш блока

        Args:
            bts (bytearray):

        Returns:
            str: sha256 представление блока

        """
        h = self._hash_func()
        h.update(bts)
        return h.hexdigest()

    @property
    def good(self):
        """Доступно хранилище для записи/чтения

        Returns:
            bool:
        """
        return self._good
