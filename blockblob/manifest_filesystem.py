# coding: utf-8
from __future__ import unicode_literals
import logging
import os

from manifest import Manifest


class ManifestFileSystem(Manifest):
    """ Осуществляет поиск по хранилищу и дедупликацию данных с помощью мета файла содержащего всю структуру связей блобов и блоков

    Args:
        path (str): Путь до директории хранилища, если директория отсутсвует, то она НЕ будет создана и все операции поиска, вставки будут возвращать 0
        meta_file (str): Путь до местоположения метафайла, если файл отсутсвует, то он НЕ будет создан и все операции поиска, вставки будут возвращать 0

    """

    def __init__(self, path='/tmp', meta_file='/tmp/.meta', *args, **kwargs):
        self._meta_file = None
        self._path = path
        self._meta_file_path = meta_file
        super(ManifestFileSystem, self).__init__(*args, **kwargs)

    def _check(self):
        result = filter(lambda x: x is False, [
            os.path.exists(self._path),
            os.path.exists(self._meta_file_path),
            os.access(self._path, os.W_OK),
            os.access(self._meta_file_path, os.W_OK),
        ])

        self._good = not bool(result)
        if not self._good:
            logging.critical('Check failed: maybe %s or %s not exists or not writeable?', self._path, self._meta_file_path)

    def store_block(self, blob_id, hashed, position, block_id):
        with open(self._meta_file_path, 'a') as f:
            f.write('{0}\n'.format(' '.join(map(str, (blob_id, hashed, position, block_id)))))

    def search_block(self, block_id, hashed=None):
        with open(self._meta_file_path, 'r') as f:
            for n, l in enumerate(f.readlines()):
                try:
                    stored_blob_id, stored_hashed, position, stored_block_id = l.strip().split(' ', 3)
                except ValueError:
                    logging.critical('Manifest file corrupted, line number: %s ', n)
                else:
                    position = int(position)
                    if str(stored_block_id) == str(block_id):
                        logging.info('%s founded in storage by block id', block_id)
                        return stored_blob_id, position
                    elif hashed and str(stored_hashed) == hashed:
                        logging.info('%s[%s] founded in storage by hash', block_id, hashed)
                        self.store_block(stored_blob_id, hashed, position, block_id)
                        return stored_blob_id, position
        return 0

    @property
    def path(self):
        return self._path
